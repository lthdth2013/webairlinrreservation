'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'myApp.version',
  'jkuri.datepicker'
]);

app.factory('myService', function() {
    var savedData = {};
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});

app.factory('myService1', function() {
    var savedData = {};
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});

app.factory('myService2', function() {
    var savedData = {};
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});

app.factory('myService3', function() {
    var savedData = {};
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});

app.factory('myService4', function() {
    var savedData = {};
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {

  $routeProvider.when('/home',
      {
          templateUrl:'partials/ticket.html',
          controller:'appController'
      }
  )
      .when('/customer',
      {
          templateUrl:'partials/customer.html',
          controller:'appController'
      }
  )
      .when('/confirm',
          {
              templateUrl:'partials/confirm.html',
              controller:'appController'
          }
      )

      .when('/history',
          {
              templateUrl:'partials/history.html',
              controller:'appController'
          }
      )

      .otherwise({
          redirectTo: '/home'
      });
}]);

app.controller('appController', ['$scope', 'myService', 'myService1', 'myService2', 'myService3', 'myService4', '$http', function ($scope, myService, myService1, myService2, myService3,myService4, $http) {

    var flightDirection = 2;
    var MSBD;
    var MSBDD;
    var GiaVeDi, ThoiGianDi, SoNguoiDi = null;
    var GiaVeVe, ThoiGianVe, SoNguoiVe = null;
    var MDCDi = null;
    var MDCVe = null;
    console.log(MDCDi);
    console.log(MDCVe);
    var DanhXung, Ho, Ten;
    var Q = [];
    $scope.isChooseKH = "btn-info";
    $scope.isChooseMC = "btn-secondary";
    $scope.portfolio = "";
    document.getElementById("DiemDen").disabled = true;
    $scope.isPortfolio = "hidden";
    var isLoginFace = 0;
    var isLoginG = localStorage.getItem("isLogin");
    if (isLoginG == 1) {
        $scope.isLogin = "hidden";
        $scope.isLogout = "show";
    }else {
        $scope.isLogin = "show";
        $scope.isLogout = "hidden";
    }
    console.log("Google " + isLoginG);
    $scope.setFlightDirectionKH = function () {
        flightDirection = 2;

        $scope.isVisibleProfile = "show";
        console.log("show");
        if ($scope.isChooseKH == "btn-info"){
            $scope.isChooseKH = "btn-secondary";
            $scope.isChooseMC = "btn-info";
        }else {
            $scope.isChooseKH = "btn-info";
            $scope.isChooseMC = "btn-secondary";
        }
    };

    $scope.setFlightDirectionMC = function () {
        flightDirection = 1;

        $scope.isVisibleProfile = "hidden";
        console.log("hidden");
        if ($scope.isChooseMC == "btn-info"){
            $scope.isChooseMC = "btn-secondary";
            $scope.isChooseKH = "btn-info";
        }else {
            $scope.isChooseMC = "btn-info";
            $scope.isChooseKH = "btn-secondary";
        }
    };

    $scope.loadFlightTo = function () {
        $scope.FlightToMBs = null;
        $scope.FlightToMTs = null;
        $scope.FlightToMNs = null;

        $http.get('http://serverairlinereservation-95082.app.xervo.io/san-bay-di').then(function (response) {
            if(response.data!=null){
                var d = response.data[0];
                console.log(d);
                $scope.FlightToMBs = d.MienBac;
                $scope.FlightToMTs = d.MienTrung;
                $scope.FlightToMNs = d.MienNam;
            }
        });
    };

    $scope.loadFlightArrive = function () {
        $scope.FlightArriveMBs = null;
        $scope.FlightArriveMTs = null;
        $scope.FlightArriveMNs = null;

        var arrive = {
            "MaSanBayDi": MSBD
        };
        console.log(arrive);
        $http.get('http://serverairlinereservation-95082.app.xervo.io/san-bay-den', {params: arrive}).then(function (response) {
            if(response.data != null){
                var d = response.data.SanBayDen;
                $scope.FlightArriveMBs = d.MienBac;
                $scope.FlightArriveMTs = d.MienTrung;
                $scope.FlightArriveMNs = d.MienNam;
            }
        });
    };

    $scope.setFlightTo = function (code,name) {
        console.log(code);
        $scope.go = name;
        MSBD = code;
        $scope.goID = code;
        document.getElementById("DiemDen").disabled = false;
    };

    $scope.setFlightArrive = function (code,name) {
        console.log(code);
        $scope.arrive = name;
        MSBDD = code;
        $scope.arriveID = code;
    };

    $scope.navigationUrl = function () {
        console.log("click");
        window.open("login/login.html","","");
    };

    $scope.getDateTo = function(dateTo){
        var str = dateTo.split("/");
        var d = new Date(str[2], str[1], str[0], 0, 0, 0, 0);
        console.log(d);
        var myEpoch = d.getTime()/1000.0 + 25200;
        console.log(myEpoch);
    };

    $scope.find = function (flight) {
        var soLuongNguoi = 0;
        if(flightDirection == 2){
            if(MSBD==null||MSBDD==null||flight.dateTo==null||flight.dateArrive==null||flight.people==null||flight.ticketCategories==null) {
                //alert("Bạn vui lòng chọn đầy đủ thông tin trước khi tìm kiếm !!");
                $scope.messageError = "Bạn vui lòng chọn đầy đủ thông tin trước khi tìm kiếm !!";
                return;
            }
            $scope.messageError = "";
            $scope.portfolio = "Kết quả tìm kiếm";

            console.log(flightDirection);
            console.log(MSBD);
            console.log(MSBDD);
            console.log(flight);

            var str = flight.dateTo;
            //console.log("Time" + str);
            //var human = new Date(str[2], str[1], str[0], 0, 0, 0, 0);
            var myEpoch = str.getTime()/1000.0 + 25200;
            console.log(myEpoch);

            var find = {
                "MaSanBayDi": MSBD,
                "MaSanBayDen": MSBDD,
                "NgayBay": myEpoch,
                "SoNguoi": flight.people,
                "Hang": flight.ticketCategories
            };
            soLuongNguoi = flight.people;
            SoNguoiDi = flight.people;
            console.log(find);
            $http.get('http://serverairlinereservation-95082.app.xervo.io/tim-chuyen-bay', {params: find}).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);
                    $scope.FlightGoS = d;
                }
            });

            var str = flight.dateArrive;
            console.log(str);
            //var human = new Date(str[2], str[1], str[0], 0, 0, 0, 0);
            var myEpoch = str.getTime()/1000.0 + 25200;
            console.log(myEpoch);

            find = {
                "MaSanBayDi": MSBDD,
                "MaSanBayDen": MSBD,
                "NgayBay": myEpoch,
                "SoNguoi": flight.people,
                "Hang": flight.ticketCategories
            };
            SoNguoiVe = flight.people;
            console.log(find);
            $http.get('http://serverairlinereservation-95082.app.xervo.io/tim-chuyen-bay', {params: find}).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);
                    $scope.FlightArriveS = d;
                }
            });
        }
        else if(flightDirection == 1)
        {
            if(MSBD==null||MSBDD==null||flight.dateTo==null||flight.people==null||flight.ticketCategories==null) {
                //alert("Bạn vui lòng chọn đầy đủ thông tin trước khi tìm kiếm !!");
                $scope.messageError = "Bạn vui lòng chọn đầy đủ thông tin trước khi tìm kiếm !!";
                return;
            }
            $scope.messageError = "";
            $scope.portfolio = "Kết quả tìm kiếm";
            $scope.isPortfolio = "show";
            var str = flight.dateTo;
            console.log(str);
            //var human = new Date(str[2], str[1], str[0], 0, 0, 0, 0);
            var myEpoch = str.getTime()/1000.0 + 25200;
            console.log(myEpoch);

            var find = {
                "MaSanBayDi": MSBD,
                "MaSanBayDen": MSBDD,
                "NgayBay": myEpoch,
                "SoNguoi": flight.people,
                "Hang": flight.ticketCategories
            };
            soLuongNguoi = flight.people;
            SoNguoiDi = flight.people;
            console.log(find);
            $http.get('http://serverairlinereservation-95082.app.xervo.io/tim-chuyen-bay', {params: find}).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);
                    $scope.FlightGoS = d;
                }
            });

        }
        myService4.set(soLuongNguoi);
    };

    $scope.getTicketTo = function (GiaBan, flight) {
        console.log(GiaBan);
        GiaVeDi = GiaBan;
        ThoiGianDi = flight.NgayBay;
        if (typeof(Storage) !== "undefined") {
            // Store
            localStorage.setItem("flightTo", flight);
            localStorage.setItem("GiaBanTo", GiaBan);
        } else {
            alert("Sorry, your browser does not support Web Storage...");
        }
        myService1.set(flight);
    };

    $scope.getTicketArrive = function (GiaBan, flight) {
        console.log(GiaBan);
        GiaVeVe = GiaBan;
        ThoiGianVe = flight.NgayBay;
        if (typeof(Storage) !== "undefined") {
            // Store
            localStorage.setItem("flightArrive", flight);
            localStorage.setItem("GiaBanArrive", GiaBan);
        } else {
            alert("Sorry, your browser does not support Web Storage...");
        }
        myService2.set(flight);
    };

    $scope.getMDC = function () {
        var a = [];
        if (GiaVeDi!=null){
            var find = {
                "ThoiGian": ThoiGianDi,
                "GiaBan": GiaVeDi,
                "SoNguoi": SoNguoiDi
            };
            console.log(find);
            $http.post('http://serverairlinereservation-95082.app.xervo.io/thong-tin-dat-cho',  find).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);
                    MDCDi = d;
                    //myService.set(MDCDi);
                    a.push(MDCDi);
                    myService.set(a);
                }
            });
        }
        if (GiaVeVe!=null){
            var find = {
                "ThoiGian": ThoiGianVe,
                "GiaBan": GiaVeVe,
                "SoNguoi": SoNguoiVe
            };
            console.log(find);
            $http.post('http://serverairlinereservation-95082.app.xervo.io/thong-tin-dat-cho', find).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);
                    MDCVe = d;
                    //myService.set(MDCVe);
                    a.push(MDCVe);
                    myService.set(a);
                }
            });
        }
        $scope.portfolio = "Thông tin khách hàng";
    };

    $scope.getInfoCustomer = function () {
        console.log($scope.danhXung);
        console.log($scope.ho);
        console.log($scope.ten);
        $scope.portfolio = "Xác nhận đặt vé";
        var MDCTH = myService.get();
        console.log(myService.get());
        DanhXung = $scope.danhXung;
        Ho = $scope.ho;
        Ten = $scope.ten;
        //console.log($scope.MDCVe);
        if (MDCTH[0]!=null){
            var find = {
                "DanhXung": $scope.danhXung,
                "Ho": $scope.ho,
                "Ten": $scope.ten,
                "MaDatCho": MDCTH[0].MaDatCho
            };
            console.log(find);
            $http.post('http://serverairlinereservation-95082.app.xervo.io/danh-sach-hanh-khach',  find).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);

                }
            });
        }
        if (MDCTH[1]!=null){
            var find = {
                "DanhXung": $scope.danhXung,
                "Ho": $scope.ho,
                "Ten": $scope.ten,
                "MaDatCho": MDCTH[1].MaDatCho
            };
            console.log(find);
            $http.post('http://serverairlinereservation-95082.app.xervo.io/danh-sach-hanh-khach', find).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log(d);
                }
            });
        }
        myService3.set(MDCTH);
        myService.set($scope);
    };

    var tempScope = myService.get();
    var tempScope1 = myService1.get();
    var tempScope2 = myService2.get();
    console.log("tempScope " +tempScope);
    if (tempScope.danhXung != undefined) {
        $scope.danhXung = tempScope.danhXung;
        $scope.ho = tempScope.ho;
        $scope.ten = tempScope.ten;

        console.log("tempScope1 " +tempScope1.MaChuyenBay);
        $scope.MCBDI = tempScope1.MaChuyenBay;
        $scope.GKHDI = tempScope1.GioKhoiHanh;
        $scope.GDNDI = tempScope1.GioDen;
        $scope.LVDI = tempScope1.MucGia;
        $scope.GVDI = tempScope1.GiaBan;

        $scope.MCBDEN = tempScope2.MaChuyenBay;
        $scope.GKHDEN = tempScope2.GioKhoiHanh;
        $scope.GDNDEN = tempScope2.GioDen;
        $scope.LVDEN = tempScope2.MucGia;
        $scope.GVDEN = tempScope2.GiaBan;
    }



    console.log("tempScope " +tempScope.danhXung);
    console.log("Scope " +$scope.danhXung);

    $scope.confirm = function () {
        var MDCTH = myService3.get();
        var temp1 = myService1.get();
        var temp2 = myService2.get();
        var soLuongNguoi = myService4.get();
        var id_token = localStorage.getItem("idToken");
        if (MDCTH[0]!=null){
            var find = {
                "MaChuyenBay": temp1.MaChuyenBay,
                "MaDatCho": MDCTH[0].MaDatCho,
                "Ngay": temp1.NgayBay,
                "Hang": temp1.Hang,
                "MucGia": temp1.MucGia,
                "SoNguoi":soLuongNguoi
            };
            console.log(find);
            $http.post('http://serverairlinereservation-95082.app.xervo.io/thong-tin-chi-tiet-dat-cho',  find).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log("xacnhan"+d);

                }
            });

            if (id_token != null){
                var find = {
                    "MaChuyenBay": temp1.MaChuyenBay,
                    "MaDatCho": MDCTH[0].MaDatCho,
                    "Ngay": temp1.NgayBay,
                    "Hang": temp1.Hang,
                    "MucGia": temp1.MucGia,
                    "SoNguoi":soLuongNguoi,
                    "ToKen":id_token
                };
                console.log(find);
                $http.post('http://serverairlinereservation-95082.app.xervo.io/history',  find).then(function (response) {
                    if(response.data != null){
                        var d = response.data;
                        console.log("Token "+d);

                    }
                });
            }
        }
        if (MDCTH[1]!=null){
            var find = {
                "MaChuyenBay": temp2.MaChuyenBay,
                "MaDatCho": MDCTH[1].MaDatCho,
                "Ngay": temp2.NgayBay,
                "Hang": temp2.Hang,
                "MucGia": temp2.MucGia,
                "SoNguoi":soLuongNguoi
            };
            console.log(find);
            $http.post('http://serverairlinereservation-95082.app.xervo.io/thong-tin-chi-tiet-dat-cho', find).then(function (response) {
                if(response.data != null){
                    var d = response.data;
                    console.log("xacnhan"+d);
                }
            });

            if (id_token != null){
                var find = {
                    "MaChuyenBay": temp2.MaChuyenBay,
                    "MaDatCho": MDCTH[1].MaDatCho,
                    "Ngay": temp2.NgayBay,
                    "Hang": temp2.Hang,
                    "MucGia": temp2.MucGia,
                    "SoNguoi":soLuongNguoi,
                    "ToKen":id_token
                };
                console.log(find);
                $http.post('http://serverairlinereservation-95082.app.xervo.io/history',  find).then(function (response) {
                    if(response.data != null){
                        var d = response.data;
                        console.log("Token "+d);

                    }
                });
            }
        }
    };

    $scope.getHistory = function () {
        var token = localStorage.getItem("idToken");
        var find = {
            "ToKen": token
        };
        console.log(find);
        $http.get('http://serverairlinereservation-95082.app.xervo.io/history', {params: find}).then(function (response) {
            if(response.data != null){
                var d = response.data;
                console.log(d);
                $scope.FlightHistory = d;
            }
        });
    };

// This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            $scope.isLogin = "hidden";
            $scope.testAPI();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            $scope.isLogin = "show";
            myService4.set("show");
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            $scope.isLogin = "show";
            myService4.set("show");
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '138285400002750',
            cookie     : true,  // enable cookies to allow the server to access
                                // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.8' // use graph api version 2.8
        });

        // Now that we've initialized the JavaScript SDK, we call
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

    };

    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    $scope.testAPI = function() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
            console.log('Successful login for: ' + response.name);
            isLoginFace = 1;
            myService4.set("hidden");
            $scope.isLogin = "hidden";
            setIsLogin();
        });
    };



    function setIsLogin(){
        if (isLoginFace == 1){
            $scope.isLogin = "hidden";
        }
    }

    console.log("Login " + $scope.isLogin);


}]);



