/**
 * Created by Ngo Chi Hai on 10/23/2016.
 */

// Declare app level module which depends on views, and components
var app = angular.module('myAppAdmin', [
    'ngRoute',
    'jkuri.datepicker',
    'myAppLogin'
]);
app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.when('/',
        {
            templateUrl:'admin.html',
            controller:'appControllerAdmin'
        }
    )

}]);


app.controller('appControllerAdmin', ['$scope',  '$http', function ($scope, $http) {
    var token = sessionStorage.getItem("token");
    console.log(sessionStorage.getItem("token"));
    $scope.loadData = function () {
        $http.get('http://serverairlinereservation-95082.app.xervo.io/danh-sach-chang-bay?token=' + token).then(function (response) {
            if(response.data!=null){
                var d = response.data;
                //console.log(d);
                for(var i=0; i<d.length;i=i+1){
                    var day = new Date(d[i].NgayBay*1000 + 2566800);
                    //console.log(day.toLocaleDateString());
                    d[i].NgayBay = day.toLocaleDateString();
                }

                $scope.Flights = d;
            }
        });

        $http.get('http://serverairlinereservation-95082.app.xervo.io/thong-tin-chi-tiet-dat-cho?token=' + token).then(function (response) {
            if(response.data!=null){
                var d = response.data;

                for(var i=0; i<d.length;i=i+1){
                    var day = new Date(d[i].Ngay*1000 + 2566800);
                    //console.log(day.toLocaleDateString());
                    d[i].Ngay = day.toLocaleDateString();
                }

                $scope.Bookings = d;
            }
        });
    };
    $scope.loadData();
    var MSBD;
    var MSBDD;
    $scope.setFlightTo = function (code,name) {
        console.log(code);
        $scope.go = name;
        MSBD = code;
        $scope.goID = code;
    };

    $scope.setFlightArrive = function (code,name) {
        console.log(code);
        $scope.arrive = name;
        MSBDD = code;
        $scope.arriveID = code;
    }

    $scope.loadFlightTo = function () {
        $scope.FlightToMBs = null;
        $scope.FlightToMTs = null;
        $scope.FlightToMNs = null;

        $http.get('http://serverairlinereservation-95082.app.xervo.io/san-bay-di').then(function (response) {
            if(response.data!=null){
                var d = response.data[0];
                $scope.FlightToMBs = d.MienBac;
                $scope.FlightToMTs = d.MienTrung;
                $scope.FlightToMNs = d.MienNam;
            }
        });
    };

    $scope.loadFlightArrive = function () {
        $scope.FlightArriveMBs = null;
        $scope.FlightArriveMTs = null;
        $scope.FlightArriveMNs = null;

        var arrive = {
            "MaSanBayDi": MSBD
        };
        console.log(arrive);
        $http.get('http://serverairlinereservation-95082.app.xervo.io/san-bay-den', {params: arrive}).then(function (response) {
            if(response.data != null){
                var d = response.data.SanBayDen;
                $scope.FlightArriveMBs = d.MienBac;
                $scope.FlightArriveMTs = d.MienTrung;
                $scope.FlightArriveMNs = d.MienNam;
            }
        });
    };
    var btnKH = document.getElementById("btnKH");
    var btnMC = document.getElementById("btnMC");
    $scope.setFlightDirectionKH = function () {
        flightDirection = 2;
        btnKH.style.backgroundColor = "#207DA5";
        btnMC.style.backgroundColor = "white";
        $scope.isVisibleProfile = "show";
        console.log("show")
    };

    $scope.setFlightDirectionMC = function () {
        flightDirection = 1;
        btnMC.style.backgroundColor = "#207DA5";
        btnKH.style.backgroundColor = "white";
        $scope.isVisibleProfile = "hidden";
        console.log("hidden")
    };
    
    $scope.addFlight = function (flight) {
        var str = flight.dateTo.split("/");
        //console.log(str);
        var human = new Date(str[2], str[1], str[0], 0, 0, 0, 0);
        var myEpoch = human.getTime()/1000.0 - 2566800;
        console.log($scope.timesGo);
        dulieu = {
            "MaChuyenBay": flight.id,
            "NgayBay": myEpoch,
            "MaSanBayDi": MSBD,
            "MaSanBayDen": MSBDD,
            "TenSanBayDi": $scope.go,
            "TenSanBayDen": $scope.arrive,
            "GioKhoiHanh": flight.timesGo,
            "GioDen": flight.timesArrive,
            "Hang": flight.ticketCategories,
            "MucGia": flight.MG,
            "SoLuongGhe": flight.people,
            "GiaBan": flight.G
        };

        $http.post('http://serverairlinereservation-95082.app.xervo.io/danh-sach-chang-bay?token=' + token,  dulieu).then(function (response) {
            if(response.data != null){
                $scope.loadData();
            }
        });
    };
    var idDelete = null;
    $scope.getTicketTo = function (flight) {
        idDelete = flight._id;
    };

    $scope.deleteFlight = function () {
        $http.delete('http://serverairlinereservation-95082.app.xervo.io/danh-sach-chang-bay?token=' + token + "&_id=" + idDelete).then(function (response) {
            if(response.data != null){
                idDelete=null;
                $scope.loadData();
            }
        });
    };

    $scope.updateFlight = function (flight) {
        var str = flight.dateTo.split("/");
        //console.log(str);
        var human = new Date(str[2], str[1], str[0], 0, 0, 0, 0);
        var myEpoch = human.getTime()/1000.0 - 2566800;
        console.log($scope.timesGo);
        dulieu = {
            "_id": idDelete,
            "MaChuyenBay": flight.id,
            "NgayBay": myEpoch,
            "MaSanBayDi": MSBD,
            "MaSanBayDen": MSBDD,
            "TenSanBayDi": $scope.go,
            "TenSanBayDen": $scope.arrive,
            "GioKhoiHanh": flight.timesGo,
            "GioDen": flight.timesArrive,
            "Hang": flight.ticketCategories,
            "MucGia": flight.MG,
            "SoLuongGhe": flight.people,
            "GiaBan": flight.G
        };
        console.log(idDelete);
        $http.put('http://serverairlinereservation-95082.app.xervo.io/danh-sach-chang-bay?token=' + token, dulieu).then(function (response) {
            if(response.data != null){
                idDelete=null;
                $scope.loadData();
            }
        });
    }
}]);