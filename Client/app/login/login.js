/**
 * Created by Ngo Chi Hai on 10/31/2016.
 */
var app = angular.module('myAppLogin', [
    'ngRoute'
]);
app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $routeProvider.when('/',
        {
            templateUrl:'login.html',
            controller:'appControllerLogin'
        }
    )
}]);



app.controller('appControllerLogin',['$scope',  '$http', function ($scope, $http) {
    $scope.signin = function () {
        dulieu = {
            "name": $scope.email,
            "password": $scope.password
        };
       // console.log(dulieu);
        $http.post('http://serverairlinereservation-95082.app.xervo.io/authenticate',  dulieu).then(function (response) {
            if(response.data != null){
                console.log(response.data);
                $scope.mess = "Đăng nhập thành công";
                sessionStorage.setItem("token",response.data.token);
                //document.cookie = "token=" + response.data.token;
                window.open("../admin/admin.html","","");
            }
        });
    }
}]);