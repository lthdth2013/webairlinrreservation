var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb").MongoClient;
var ObjectID = require("mongodb").ObjectID;
var app = express();
var jwt = require("jsonwebtoken");

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var route = require("./route/route.js")(app,mongodb,jwt,ObjectID);

var server = app.listen(8080, function () {
    console.log("Listening on port %s...", server.address().port);
});