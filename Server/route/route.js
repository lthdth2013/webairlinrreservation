var db;

var appRoute = function (app,mongodb,jwt,ObjectID) {
    // connect to mongodb
    mongodb.connect('mongodb://letanvu:123@ds063946.mlab.com:63946/airlinereservations',function(err,database){
            if (err) {
                console.log('connection to mongodb fail');
          } else { 
                  db = database;    
                  console.log('connection to mongodb succesful');
          }
    });
    
    app.get("/", function (req, res) {    
        res.status(200).json("Welcome to Airline Reservation"); 
    });

    
    var user = {
        username: "admin",
        password:"admin"
    };
    
    var superSecret = "ilovescotchyscotch";
    
    app.post("/authenticate", function(req,res){
        if(req.body.name != user.username || req.body.password != user.password){
            res.status(401).json({"error":"Login fail"});
        }else{
            
            var token = jwt.sign(user, superSecret);
            res.status(200).json({"token": token});
        }
    });
    
    
    /*
        1: API lay danh sach san bay di
    */
    
    app.get("/san-bay-di", function (req, res) {
        db.collection('Airport').find({}).toArray(function(err,airports){
            if(err){
                res.status(404).json({"error":"Not found"});
            }else{        
                res.status(200).json(airports); 
            }
        });
    });

    /*
       2: API lay danh sach san bay den dua vao san bay di
    */
    
    app.get("/san-bay-den", function (req, res) {
            db.collection('DestinationAirport').findOne({MaSanBayDi:req.param('MaSanBayDi')},function(err,airport){
            if(err){
                res.status(404).json({"error":"Not found"});
            }else{
                res.status(200).json(airport); 
            }
        });
    });
 
    
    /*
       3: API lay danh sach hanh khach co cung ma dat cho
    */
    
    app.get("/danh-sach-hanh-khach/:MaDatCho", function (req, res) {
            db.collection('Passenger').find({MaDatCho:parseInt(req.params.MaDatCho)}).toArray(function(err,passenger){
            if(err){
                res.status(404).json({"error":"Not found"});
            }else{
                res.status(200).json(passenger); 
            }
        });
    });
    
    
    
    
    
    
    /*
       4: API them hanh khach moi
    */
    
    app.post("/danh-sach-hanh-khach", function (req, res) {
        if(!(req.body.DanhXung || req.body.Ho || req.body.MaDatCho || req.body.Ten)){
            res.status(500).json({"error":"Invalid input"});
        }   
      
        db.collection('Passenger').insert(req.body, function(err,passenger2){
            if(err){
                res.status(501).json({"error":"Add passenger fail"});
            }else{
                res.status(200).json({"message": "Add passenger successfull"}); 
            }
        });
    });
    
    
     /*
       5: API lay danh sach chang bay danh cho admin
    */
    
    app.get("/danh-sach-chang-bay", function (req, res) {
        var token = req.param('token');
        if(token){
             jwt.verify(token, superSecret, function(err, decoded) {      
              if (err) {
                 res.status(401).json({"error":"Token fail"}); 
              } else {
                        db.collection('Flight').find({}).toArray(function(err,flight){
                            if(err){
                                res.status(404).json({"error":"Not found"});
                            }else{
                                res.status(200).json(flight); 
                            }
                        });
              }
            });
        }else{
             res.status(401).json({"error":"Required token"}); 
        }     
    });
    
    /*
       12: API xoa danh sach chang bay danh cho admin
    */
    
    app.delete("/danh-sach-chang-bay", function (req, res) {
        var token = req.param('token');
        if(token){
             jwt.verify(token, superSecret, function(err, decoded) {      
              if (err) {
                 res.status(401).json({"error":"Token fail"}); 
              } else {
                        db.collection('Flight').remove({_id:ObjectID(req.param('_id'))},function(err,flight){
                            if(err){
                                res.status(404).json({"error":"Not found"});
                            }else{
                                res.status(200).json({"message":"Delete successfull"}); 
                            }
                        });
              }
            });
        }else{
             res.status(401).json({"error":"Required token"}); 
        }     
    });
    
      
    /*
       13: API sua danh sach chang bay danh cho admin
    */
    
    app.put("/danh-sach-chang-bay", function (req, res) {
        var token = req.param('token');
        if(token){
             jwt.verify(token, superSecret, function(err, decoded) {      
              if (err) {
                 res.status(401).json({"error":"Token fail"}); 
              } else {
                        db.collection('Flight').remove({_id:ObjectID(req.body._id)},function(err,flight){
                            if(err){
                                res.status(404).json({"error":"Not found"});
                            }else{
                                    var updatedocument = req.body;
                                    updatedocument._id=ObjectID(req.body._id);
                                    db.collection('Flight').insert(updatedocument, function(err,flight){
                                        if(err){
                                            res.status(501).json({"error":"Update flight fail"});
                                        }else{
                                            res.status(200).json({"message": "Update flight successfull"}); 
                                        }
                                    });         
                            }
                        });
                    }
            });
        }else{
             res.status(401).json({"error":"Required token"}); 
        }     
    });
    
    
     /*
       6: API them chang bay danh cho admin
    */
    
    app.post("/danh-sach-chang-bay", function (req, res) {
        var token = req.param('token');
        if(token){
             jwt.verify(token, superSecret, function(err, decoded) {      
              if (err) {
                 res.status(401).json({"error":"Token fail"}); 
              } else {
                       
                    if(!(req.body.MaChuyenBay || req.body.NgayBay || req.body.MaSanBayDi || req.body.MaSanBayDen ||
                        req.body.TenSanBayDi || req.body.TenSanBayDen || req.body.GioKhoiHanh || req.body.GioDen
                        ||req.body.Hang || req.body.MucGia || req.body.SoLuongGhe || req.body.GiaBan)){
                        res.status(500).json({"error":"Invalid input"});
                    }

                    db.collection('Flight').insert(req.body, function(err,flight){
                        if(err){
                            res.status(501).json({"error":"Add flight fail"});
                        }else{
                            res.status(200).json({"message": "Add flight successfull"}); 
                        }
                    });  
              }
            });
        }else{
             res.status(401).json({"error":"Required token"}); 
        }  
    });
    
    
    
      /*
       7: API lay thong tin dat cho cho hanh khach
    */
    
    app.get("/thong-tin-dat-cho:MaDatCho", function (req, res) {
            db.collection('Booking').find({MaDatCho:parseInt(req.params.MaDatCho)}).toArray(function(err,booking){
            if(err){
                res.status(404).json({"error":"Not found"});
            }else{
                res.status(200).json(booking); 
            }
        });
    });
    
    
     /*
       8: API tao ma dat cho moi
    */
    
    app.post("/thong-tin-dat-cho", function (req, res) {
        // check data
        if(!(req.body.ThoiGian || req.body.GiaBan || req.body.SoNguoi)){
            res.status(500).json({"error":"Invalid input"});
        }
        // tao ma dat cho
        db.collection('Booking').find({}).toArray(function(err,booking){
            if(err){
                res.status(501).json({"error":"Create booking fail"});
            }else{
                var code = booking.length;
                var newBooking={
                    "MaDatCho": code+1,
                    "ThoiGianDatCho":req.body.ThoiGian,
                    "TongTien":req.body.SoNguoi*req.body.GiaBan,
                    "TrangThai":0
                };
                db.collection('Booking').insert(newBooking, function(err,booking){
                    if(err){
                         res.status(501).json({"error":"Create booking fail"});
                    }else{
                        res.status(200).json({"MaDatCho": code+1}); 
                    }
                });
            }
        });
    });
    
     /*
       9: API them chi tiet ma dat cho va
       Cap nhat lai so ghe con lai cua chuyen bay vua dat
       Cap nhat lai trang thai dat cho la 1
    */
    
    
    app.post("/thong-tin-chi-tiet-dat-cho", function (req, res) {
        if(!(req.body.MaChuyenBay || req.body.Ngay || req.body.MaDatCho || req.body.Hang ||
            req.body.MucGia ||req.body.SoNguoi)){
            res.status(500).json({"error":"Invalid input"});
        }
        
        var seat;
        var newDetail = {
                "MaDatCho" : req.body.MaDatCho,
                "MaChuyenBay" : req.body.MaChuyenBay,
                "Ngay" : req.body.Ngay,
                "Hang" : req.body.Hang,
                "MucGia" : req.body.MucGia 
        };
        
        // update number of seat
        db.collection('Flight').findOne({MaChuyenBay:req.body.MaChuyenBay,NgayBay:req.body.Ngay,
                                              Hang:req.body.Hang,MucGia:req.body.MucGia},function(err,flight){
                    if(err){
                        res.status(501).json({"error":"Update number of seat fail"});
                    }else{
                        seat = flight.SoLuongGhe - req.body.SoNguoi;
                       
                        db.collection('Flight').update({MaChuyenBay:req.body.MaChuyenBay,NgayBay:req.body.Ngay,Hang:req.body.Hang,MucGia:req.body.MucGia},
                                                            {$set:{SoLuongGhe:seat}});
                    }
        });  
        
        // update status booking
        db.collection('Booking').update({MaDatCho:req.body.MaDatCho},{$set:{TrangThai:1}});
        
        // insert new booking detail
        db.collection('BookingDetail').insert(newDetail, function(err,bookingDetail){
            if(err){
                res.status(501).json({"error":"Add booking detail fail"});
            }else{
                res.status(200).json({"message": "Add booking detail successfull"}); 
            }
        });
    });
    
      /*
       10: API lay thong tin chi tiet dat cho danh cho admin
    */

    app.get("/thong-tin-chi-tiet-dat-cho", function (req, res) {
        
         var token = req.param('token');
        if(token){
             jwt.verify(token, superSecret, function(err, decoded) {      
              if (err) {
                 res.status(401).json({"error":"Token fail"}); 
              } else {
                     db.collection('BookingDetail').find({}).toArray(function(err,bookingDetail){
                    if(err){
                        res.status(404).json({"error":"Not found"});
                    }else{
                        res.status(200).json(bookingDetail); 
                    }
                });   
              }
            });
        }else{
             res.status(401).json({"error":"Required token"}); 
        }      
    });
    
    
      /*
       14: API lay thong tin chi tiet ma dat cho
    */

    app.get("/thong-tin-chi-tiet-dat-cho:MaDatCho", function (req, res) {
        db.collection('BookingDetail').find({MaDatCho:req.params.MaDatCho}).toArray(function(err,bookingDetail){
            if(err){
                res.status(404).json({"error":"Not found"});
            }else{
                res.status(200).json(bookingDetail); 
            }
    });
    });
    
       /*
       11: API tim chuyen bay
    */
    
    app.get("/tim-chuyen-bay", function (req, res) {
        if(!(req.param('NgayBay') || req.param('MaSanBayDi') || req.param('MaSanBayDen') || req.param('Hang') || req.param('SoNguoi'))){
            res.status(500).json({"error":"Invalid input"});
        }
        
        db.collection('Flight').find({NgayBay:parseInt(req.param('NgayBay')),
                                      Hang:req.param('Hang'),
                                      MaSanBayDen:req.param('MaSanBayDen'),
                                      SoLuongGhe:{$gte:parseInt( req.param('SoNguoi'))},
                                      MaSanBayDi:req.param('MaSanBayDi')}).toArray(function(err,flight){
            if(err){
                res.status(404).json({"error":"Not found"});
            }else{
                res.status(200).json(flight); 
            }
        });
    });
    

}

module.exports = appRoute;